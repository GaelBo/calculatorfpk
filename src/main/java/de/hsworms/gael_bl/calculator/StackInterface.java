package de.hsworms.gael_bl.calculator;

public interface StackInterface {

    //Methode, öffnenlich
    // es gibt eine Refactor Method, c
     void push(Object o);
     Object pop() throws IndexOutOfBoundsException;
     void clearAll();
     int size();

}
