package de.hsworms.gael_bl.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class StackImpl implements StackInterface {

    //möchte ein Objekt speichern in einem LIFO
    private Object[]  content =new Object[10];
    private Stack<Object>stack = new Stack<>();

    //Methode, öffnenlich
    // es gibt eine Refactor Method, c
    @Override //Es ist nur eine Hilfe, das Compile weiss das ich überschreibe die funktionen, und es hilfe der Entwickler zu wissen
    public void push(Object o){
        stack.push(o);
    }
    //on doit absichert le pop car , il peut avoir des erreurs lors de l 'excution
    //d'ou limplemtation des throws... exception
    @Override
    public Object pop() throws IndexOutOfBoundsException{
        Object result;
        try{
            //hier spezialisieren wir die Exception
            result = stack.pop();
        }catch (EmptyStackException emptyStackException){
            throw new IndexOutOfBoundsException();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public void clearAll() {
        stack.clear();

    }
    @Override
    public int size(){
        return stack.size();

    }

}
