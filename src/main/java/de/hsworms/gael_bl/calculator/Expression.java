package de.hsworms.gael_bl.calculator;

import de.hsworms.gael_bl.calculator.util.CharaterUtil;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;
import java.util.StringTokenizer;

public class Expression {
    private StringTokenizer stringTokenizer;
    private Token[] tokens;
    enum Type {
        LITERAL,OPERATOR
    }
    class Token {
        private String surface;
        private  Type type;

        public Token(String surface, Type type) {
            this.surface = surface;
            this.type = type;
        }

        public String getSurface() {
            return surface;
        }

        public void setSurface(String surface) {
            this.surface = surface;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }
    } //End token class

    //konstruktur grosse Class
    public Expression(String input ) {
        stringTokenizer = new StringTokenizer(input);
        evaluate();
    }

    private Token[]evaluate(){
        tokens = new Token[100];
        int counter = 0;
        while (stringTokenizer.hasMoreTokens()){
            String item = stringTokenizer.nextToken();

            if (CharaterUtil.isDigit(item.charAt(0))){
                tokens[counter] = new Token(item,Type.LITERAL);
                counter++;
            } else if (CharaterUtil.isOperator(item.charAt(0))){
                tokens[counter] = new Token(item,Type.OPERATOR);
                counter++;
            }else {
                throw new IllegalStateException("expected [0-9] or [-+/*] but got " +item);
            }
        }
        return tokens;
    }

    public Iterator<Token> getIterator(){
        return new TokenIterator();
    }
    private class TokenIterator implements Iterator<Token>{

        private int counter = 0;
        @Override
        public boolean hasNext() {
           // if (tokens[counter] != null)
            return (tokens[counter] != null) ;
        }

        @Override
        public Token next() {
            return tokens[counter++];
        }
    }
}
