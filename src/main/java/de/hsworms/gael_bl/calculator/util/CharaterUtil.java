package de.hsworms.gael_bl.calculator.util;

public class CharaterUtil {
    /*
    Wir wollen eine Class erstellen, und Charater von unser "myCollection" gut zu manipulieren
     */

    public static boolean isDigit(char input){
        String inputString =  Character.toString(input);
        //inputString.matches("[0-9]");
        return inputString.matches("[0-9]");
    }

    public static boolean isOperator(char input){
        String inputString =  Character.toString(input);
        return inputString.matches("[-+/*]");
    }
}
