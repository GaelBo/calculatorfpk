package de.hsworms.gael_bl.calculator;

import java.util.Iterator;
import java.util.StringTokenizer;

public class MyColleection implements Iterator<String>{

    private StringTokenizer stringTokenizer;

    public MyColleection(String inputString){
        stringTokenizer = new StringTokenizer(inputString);
    }
    @Override
    public boolean hasNext() {
        return stringTokenizer.hasMoreTokens() ;
    }

    @Override
    public String next() {
        return stringTokenizer.nextToken();
    }
}
