package de.hsworms.gael_bl.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StackInterfaceTest {
    //um immer der Zustand der Stack zu wissen, wo man sich finden
    //brauchen wir eine Stack instance
    private  StackInterface stack;
    @Before
    public void setUp() throws Exception {
        stack = new StackImpl();
    }

    @After
    public void tearDown() throws Exception {
        stack = null; // wenn fertig, kommt CabbageCollector
    }

    //schreiben wir mal eigene TEST
    @Test
    public void testAutoBoxing(){
        Integer one = new Integer(1);
        assertEquals(one, new Integer(1));
    }

    @Test
    public void push() throws Exception {
        stack.push(new Integer(1));
        stack.push(new Integer(2));
        assertEquals(new Integer(2),stack.pop());//prüft ob der erwartet 1 ist gleich stack.pop()
        assertEquals(1,stack.size());

    }

    @Test
    public void pop() throws Exception {
        stack.push(new Integer(1));
        stack.push(new Integer(2));
        assertEquals(new Integer(2),stack.pop());//prüft ob der erwartet 1 ist gleich stack.pop()
        assertEquals(1,stack.size());
    }
    @Test
    public void popWithException() throws Exception {
        try {
            stack.pop();
            //wenn fail() geruft wird, das heißt ein Pb getroffen wurde, da use man //fixme fail() function
            fail("Expected 'IndexOutOfBoundsException' ");
        }catch (IndexOutOfBoundsException e){
            //OK, test passed, hier ist spezialisierte Exception
        }catch (Exception ee){
            fail("Wrong Exception fired: " +ee.getMessage()); //da sehe ich was ist genau passiert

        }

    }

    @Test
    public void clearAll() throws Exception {
        stack.push(new Integer(1));
        stack.push(new Integer(2));
        stack.clearAll();
        assertEquals(0,stack.size());
    }

    private static final int TEST_SIZE = 100;
    @Test
    public void size() throws Exception {
        for (int i = 0; i <TEST_SIZE ; i++) {
            stack.push(i);

        }
        assertEquals(TEST_SIZE,stack.size());
    }

}