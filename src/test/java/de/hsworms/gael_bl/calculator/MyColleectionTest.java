package de.hsworms.gael_bl.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyColleectionTest {

    private  MyColleection myColleection;
    private static  String TEST_STRING = "The quick brown fox";
    private static String[] EXPECTED_STRINGS = {"The", "quick", "brown", "fox"};

    @Before
    public void setUp() throws Exception {
        myColleection = new MyColleection(TEST_STRING);
    }

    @After
    public void tearDown() throws Exception {
        myColleection = null;
    }

    @Test
    public void hasNext(){
        int counter = 0;
        while(myColleection.hasNext()){
            assertEquals(EXPECTED_STRINGS[counter],myColleection.next());
            counter++;
        }

    }
    @Test
    public void collectionNotEmpty() {
        assertTrue(myColleection.hasNext());


    }

}