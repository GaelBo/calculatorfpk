package de.hsworms.gael_bl.calculator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RegExTester {


    @Before
    public void setUp(){

    }
    @After
    public void tearDown(){

    }
    @Test
    public void testRegularExpressionResultTrue(){
        String inputStringWithoutCarrot = "TestStringHorribleLong 0000 <L>";
       // inputString.matches("^[^^]*$");
        Assert.assertTrue(inputStringWithoutCarrot.matches("^[^^]*$"));
    }
    @Test
    public void testRegularExpressionResultFalse(){
        String inputStringWithoutCarrot = "TestStringHorrible^Long 0000 <L>";
        // inputString.matches("^[^^]*$");
        Assert.assertFalse(inputStringWithoutCarrot.matches("^[^^]*$"));
    }
}
