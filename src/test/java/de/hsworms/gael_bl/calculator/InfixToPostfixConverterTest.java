package de.hsworms.gael_bl.calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class InfixToPostfixConverterTest {
    @Test
    public void toPostfix(){
        //ich erwatete a+b*(c-d) das heißt 4 REAL abcd und 3 OPERATOR
        assertEquals("1 2 3 4 - * +",InfixToPostfixConverter.toPostfix("1+2*(3-4)"));
    }

}