package de.hsworms.gael_bl.calculator.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CharaterUtilTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown()  {
    }

    @Test
    public void isDigit() {
        assertTrue(CharaterUtil.isDigit('1'));
        assertFalse(CharaterUtil.isDigit('*'));
    }

    @Test
    public void isOperator() {
        assertTrue(CharaterUtil.isOperator('*'));
        assertFalse(CharaterUtil.isOperator('1'));
    }

    /*
    Stellen sich vor, einen Kunde kriegt ärgert bei Eingabe des nomber
    Es muss direct beim Code nachgestellt werden
    //Fixme Klausur: How to Deal with Bug Reports ?
     */
    @Test
    public void  dotDoesnotWork() {
        assertFalse(CharaterUtil.isDigit('.'));
        assertFalse(CharaterUtil.isOperator('1'));
    }
}