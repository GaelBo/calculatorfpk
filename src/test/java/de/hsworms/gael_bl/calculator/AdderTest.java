package de.hsworms.gael_bl.calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class AdderTest {

    private static double DELTA = 0.00000001;
    @Test
    public void add() throws Exception {
        //Test ob, a+b=2 mit akzeptanz fehler 0.00001=DELTA
        assertEquals(2, Adder.add(1,1),DELTA);
        assertEquals(Double.MAX_VALUE,Adder.add(0.,Double.MAX_VALUE),DELTA);
    }

}